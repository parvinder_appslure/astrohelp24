import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export const SubmitButton = (title, callback) => (
  <TouchableOpacity style={styles.btn_touch} onPress={callback}>
    <LinearGradient
      colors={['#FA9219', '#F97012']}
      start={{x: 1, y: 1}}
      end={{x: 0, y: 0}}
      style={styles.btn_lineargradient}>
      <Text style={styles.btn_label}>{title}</Text>
    </LinearGradient>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  btn_touch: {
    margin: 20,
    width: '80%',
    alignSelf: 'center',
    borderRadius: 20,
    overflow: 'hidden',
  },
  btn_label: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  btn_lineargradient: {
    paddingVertical: 10,
  },
});

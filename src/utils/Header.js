import React from 'react';
import {
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export const SimpleHeader = (title, callback) => (
  <LinearGradient
    colors={['#FA9219', '#F97012']}
    start={{x: 1, y: 1}}
    end={{x: 0, y: 0}}
    style={headerStyle.container}>
    <TouchableOpacity style={headerStyle.touch} onPress={callback}>
      <Image
        source={require('../assets/backwhite.png')}
        style={headerStyle.backImage}
      />
    </TouchableOpacity>
    <Text style={headerStyle.title}>{title}</Text>
  </LinearGradient>
);

const headerStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight + 10 : 10,
    paddingBottom: 10,
  },
  title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    paddingHorizontal: 20,
  },
  backImage: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touch: {
    padding: 10,
  },
});

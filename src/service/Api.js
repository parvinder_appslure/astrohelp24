import AsyncStorage from '@react-native-community/async-storage';
import {request, requestMultipart} from './ApiSauce';
// import Geolocation from '@react-native-community/geolocation';
// import Geocoder from 'react-native-geocoder';
import {Dimensions} from 'react-native';
import {latitudeDelta} from '../service/Config';
// const request = (path, json) => {
//   return new Promise((resolve, reject) => {
//     ApiSauce.post(path, json).then((response) => {
//       if (response.ok) {
//         resolve(response.data);
//       } else {
//         console.log(response.err);
//         reject(response.err);
//       }
//     });
//   });
// };

export const SignInApi = (json) => request('signin_astrologer', json);
export const SignUpApi = (json) => request('Signup_astrologer', json);
export const OtpApi = (json) => request('otp_send_astrologer', json);
export const StatusApi = (json) => request('online_offline_status', json);
export const EarningApi = (json) => request('astrologers_earning', json);
export const HomeApi = (json) => request('home_astrologers', json);
export const NotificationApi = (json) =>
  request('list_astrologer_notification', json);
export const DynamicApi = (json) => request('astrologers_dynamic', json);
export const AcceptRejectApi = (json) => request('accept_reject_request', json);
export const HoroscopeHistoryApi = (json) => request('horoscope_history', json);
export const ForgotPasswordApi = (json) =>
  request('astrologer_forget_password_otp', json);
export const ResetPasswordApi = (json) =>
  request('change_password_astrologer', json);
export const GetProfileApi = (json) => request('get_profile_astrologers', json);
export const CheckStatusApi = (json) => request('check_all_steps', json);
export const UpdateProfileApi = (json) =>
  request('update_superviser_profile', json);
export const UpdateProfessionalDetailsApi = (json) =>
  request('update_professional_details', json);
export const MasterSpecializationApi = (json) =>
  request('master_specialization', json);
export const MasterEducationDegreeApi = (json) =>
  request('master_education_degree', json);
export const LanguageCategoriesApi = (json) =>
  request('language_categories', json);
export const MasterCityApi = (json) => request('master_city', json);
export const AttachListApi = (json) => request('list_upload_horoscope', json);

export const AttachRemoveApi = (json) =>
  request('delete_images_horoscope', json);
export const AttachCompleteApi = (json) => request('complete_horoscope', json);
export const CallHistoryApi = (json) => request('call_history', json);
export const ChatHistoryApi = (json) => request('chat_history', json);
export const VideoHistoryApi = (json) => request('video_history', json);
export const PoojaHistoryApi = (json) => request('puja_booking_history', json);
export const PoojaCouponApi = (json) => request('puja_list_for_coupan', json);
export const OtherApi = (path) => request(path, {}); //
export const CouponApi = (json) =>
  request('create_astrologer_coupans_for_user', json);
export const CancelAstrologerBookingsApi = (json) =>
  request('cancel_astrologer_bookings', json);
export const PremiumAstrologerApi = (json) =>
  request('my_schedule_bookings', json); //
// multipart
export const AttachUploadApi = (json) =>
  requestMultipart('image_attchment_upload_horoscope', json);
export const AsyncStorageSetUserId = (id) =>
  AsyncStorage.setItem('user_id', id);
export const AsyncStorageGetUserId = () => AsyncStorage.getItem('user_id');

export const AsyncStorageClear = () => AsyncStorage.clear();

export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;
export const LongitudeDelta = () =>
  (latitudeDelta * Dimensions.get('window').width) /
  Dimensions.get('window').height;
export const LatitudeDelta = latitudeDelta;

export const formatAmount = (amount) =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = (str) =>
  str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = (price) => `\u20B9 ${price}`;

export const timeFormate_mmss = (time) => {
  let mm = Math.floor(time / 60);
  let ss = time % 60;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss < 10 ? `0${ss}` : ss;
  return `${mm}:${ss}`;
};

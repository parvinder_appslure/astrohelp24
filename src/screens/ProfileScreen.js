import React, {useEffect} from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {useSelector} from 'react-redux';
import {globStyle} from '../styles/style';
import {SubmitButton} from '../utils/Button';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';

const ProfileScreen = ({navigation}) => {
  const {user} = useSelector((state) => state);
  const typeView = (type, value) => (
    <View style={styles.pro_v2}>
      <Text style={styles.pro_type}>{type}</Text>
      <Text style={styles.pro_value}>{value}</Text>
    </View>
  );
  useEffect(() => {
    console.log(JSON.stringify(user, null, 2));
  }, []);
  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      <ScrollView>
        {SimpleHeader('Profile', () => navigation.goBack())}
        <View style={styles.pro_v1}>
          <Image source={{uri: user.image}} style={styles.pro_image} />
          <Text style={styles.pro_name}>{user.name}</Text>
          <Text style={styles.pro_type}>{user.email}</Text>
        </View>
        {typeView('Mobile.', user.mobile)}
        {typeView('D.O.B.', user.dob)}
        {typeView('Gender', user.gender)}
        {typeView('Consultant Type', user.consultation_type)}
        {typeView(
          'Skill',
          user.skills.map((item) => item.skill_name).join(' | '),
        )}
        {typeView('Language', user.language)}
        {typeView('Experience', `${user.experience} years`)}
        {SubmitButton('EDIT PROFILE', () =>
          navigation.navigate('EditProfileScreen'),
        )}
      </ScrollView>
    </SafeAreaProvider>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  pro_v1: {
    alignItems: 'center',
    marginVertical: 20,
  },
  pro_v2: {
    marginBottom: 15,
    marginHorizontal: 30,
  },
  pro_image: {
    height: 100,
    width: 100,
    borderRadius: 50,
  },
  pro_name: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#1D1E2C',
  },
  pro_type: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#69707F',
  },
  pro_value: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#1D1E2C',
    paddingHorizontal: 10,
  },
});

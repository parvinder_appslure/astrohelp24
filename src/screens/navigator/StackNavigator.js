import React, {useRef} from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Splash from '../Splash';
import Login from '../Login';
import SignUp from '../SignUp';
import OtpScreen from '../OtpScreen';
import ProfessionalDetails from '../ProfessionalDetails';
import BankDetails from '../BankDetails';
import UploadDocuments from '../UploadDocuments';
import HomeScreen from '../HomeScreen';
import ProfileScreen from '../ProfileScreen';
import EditProfileScreen from '../EditProfileScreen';
import PremiumAstrologer from '../PremiumAstrologer';
import PremiumAstrologerBookingDetail from '../PremiumAstrologerBookingDetail';
import ChatHistory from '../ChatHistory';
import ChatScreen from '../ChatScreen';
import SettingScreen from '../SettingScreen';
import TotalEarning from '../TotalEarning';
import HoroscopeOrderedScreen from '../HoroscopeOrderedScreen';
import HoroscopeDetail from '../HoroscopeDetail';
import OnlinePuja from '../OnlinePuja';
import VideoCallHistory from '../VideoCallHistory';
import CallHistory from '../CallHistory';
import ForgotPassword from '../ForgotPassword';
import ResetPassword from '../ResetPassword';
import MyChat from '../MyChat';
import VideoCall from '../VideoCall';
import AudioCall from '../AudioCall';
import Coupon from '../Coupon';
import Notification from '../Notification';
import HtmlScreen from '../HtmlScreen';

const Stack = createStackNavigator();
const StackNavigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ResetPassword"
          component={ResetPassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OtpScreen"
          component={OtpScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ProfessionalDetails"
          component={ProfessionalDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="BankDetails"
          component={BankDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="UploadDocuments"
          component={UploadDocuments}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MyChat"
          component={MyChat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AudioCall"
          component={AudioCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoCall"
          component={VideoCall}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Coupon"
          component={Coupon}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfileScreen"
          component={EditProfileScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PremiumAstrologer"
          component={PremiumAstrologer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PremiumAstrologerBookingDetail"
          component={PremiumAstrologerBookingDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ChatHistory"
          component={ChatHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ChatScreen"
          component={ChatScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SettingScreen"
          component={SettingScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TotalEarning"
          component={TotalEarning}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HoroscopeOrderedScreen"
          component={HoroscopeOrderedScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HoroscopeDetail"
          component={HoroscopeDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OnlinePuja"
          component={OnlinePuja}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="VideoCallHistory"
          component={VideoCallHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CallHistory"
          component={CallHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HtmlScreen"
          component={HtmlScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;

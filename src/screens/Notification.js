import React, {useEffect, useState} from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {useStore} from 'react-redux';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import {NotificationApi} from '../service/Api';
const Notification = ({navigation, route}) => {
  const store = useStore();
  const user_id = store.getState().user.user_id;
  const [state, setstate] = useState({
    isLoading: true,
    list: [],
  });
  useEffect(() => {
    (async () => {
      console.log(user_id);
      const {status = false, list = []} = await NotificationApi({user_id});
      if (!status) {
      }
      setstate({...state, list, isLoading: false});
    })();
  }, []);
  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {SimpleHeader('Notification', () => navigation.goBack())}
      {state.list.length === 0 && !state.isLoading && (
        <Text style={styles.noResult}>No Notification Found</Text>
      )}
      <FlatList
        data={state.list}
        contentContainerStyle={{
          paddingTop: 20,
        }}
        renderItem={({item}) => (
          <View style={styles.fl_container}>
            <Image
              style={styles.fl_image}
              source={require('../assets/bell11.png')}
            />
            <View style={styles.fl_view}>
              <Text style={styles.fl_title}>{item.title}</Text>
              <Text style={styles.fl_title}>{item.notification}</Text>
              <Text style={styles.fl_date}>{item.added_on}</Text>
            </View>
          </View>
        )}
      />
    </SafeAreaProvider>
  );
};

export default Notification;

const styles = StyleSheet.create({
  fl_container: {
    flexDirection: 'row',
    marginHorizontal: 30,
    marginBottom: 20,
    borderRadius: 15,
    borderLeftColor: '#FF9445',
    borderLeftWidth: 5,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: 'white',
    elevation: 5,
  },
  fl_image: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  fl_view: {
    marginHorizontal: 20,
  },
  fl_title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#000521',
  },
  fl_date: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#FF9445',
    marginTop: 5,
  },
  noResult: {
    marginTop: '50%',
    alignSelf: 'center',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
  },
});

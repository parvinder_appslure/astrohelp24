import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  Dimensions,
  NativeModules,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {Switch} from 'react-native-switch';
const {width, height} = Dimensions.get('window');
import React, {useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Global from './Global';
import {EventRegister} from 'react-native-event-listeners';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import Loader from '../utils/Loader';
import {useDispatch, useSelector, useStore} from 'react-redux';
import * as actions from '../redux/actions';
import {
  DynamicApi,
  HomeApi,
  AcceptRejectApi,
  StatusApi,
  MasterSpecializationApi,
} from '../service/Api';
import {useIsFocused} from '@react-navigation/native';

import requestCameraAndAudioPermission from './permission';
const {Agora} = NativeModules;
const {
  FPS30,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  Adaptative,
} = Agora;

// type Props = {};
const calltype = {
  1: 'Video Call',
  2: 'Audio Call',
  3: 'Chat',
};
const HomeScreen = ({navigation, route}) => {
  const store = useStore();
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    video: false,
    audio: false,
    chat: false,
    isLoading: false,
    user_id: store.getState().user.user_id,
    app_status: store.getState().user.app_status,
    approved: store.getState().user.approved,
    user_detail: [],
  });
  const [resultState, setResultState] = useState({
    status: false,
    is_chat_or_video_start: 0,
    booking_id: 0,
    chat_g_id: 0,
    is_booking: 0,
    user_name: '',
    booking_type: '',
    user_id: '',
    user_gender: '',
    request_array: [],
  });
  const {netInfo, user} = useSelector((store) => store);
  const dispatch = useDispatch();

  const headerView = () => (
    <LinearGradient
      colors={['#FA9219', '#F97012']}
      start={{x: 1, y: 1}}
      end={{x: 0, y: 0}}
      style={styles.header_container}>
      <View style={styles.header_icon_view}>
        <Image
          style={styles.header_icon}
          source={require('../assets/homelogo.png')}
        />
      </View>
      <View style={styles.header_titleView}>
        <Text style={styles.header_text_1}>Welcome</Text>
        <Text style={styles.header_text_2}>{user.name}</Text>
      </View>
      <TouchableOpacity
        style={styles.header_notifyTouch}
        onPress={() => navigation.navigate('Notification')}>
        <Image
          style={styles.header_notifyIcon}
          //   style={{height: 26, width: 23, resizeMode: 'contain'}}
          source={require('../assets/notificationBell.png')}
        />
      </TouchableOpacity>
    </LinearGradient>
  );
  const statusView = () => (
    <View style={styles.sv_container}>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t1}>Type</Text>
        <Text style={styles.sv_t1}>Status</Text>
      </View>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t2}>Video Call</Text>
        {switchView('video')}
      </View>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t2}>Phone Call</Text>
        {switchView('audio')}
      </View>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t2}>Chat</Text>
        {switchView('chat')}
      </View>
    </View>
  );
  const requestHandler = async (what, item) => {
    // console.log(what, item);
    toggleLoading(true);
    const body = {
      id: item.id,
      what,
    };
    console.log(body);
    const result = await AcceptRejectApi(body);
    console.log(result);
    toggleLoading(false);
  };
  const bookingView = () => (
    <>
      <Text style={styles.bv_title}>New Booking</Text>
      {resultState.request_array.map((item) => (
        <View style={styles.bv_container} key={`_id_${item.id}`}>
          {/* <View style={styles.bv_h1}>
            <Text style={styles.sv_t1}>Date: 26/11/2020</Text>
            <Text style={styles.sv_t1}>Time: 2:00 PM</Text>
          </View> */}
          <View style={styles.bv_h2}>
            <Image style={styles.bv_image} source={{uri: item.img}} />
            <View style={styles.bv_h3}>
              <Text style={styles.bv_t1}>{item.name}</Text>
              <Text style={styles.bv_t2}>{`${item.gender} | ${
                item.age || '- -'
              } yrs`}</Text>
              <Text style={styles.bv_t3}>{`Call: ${calltype[item.type]}`}</Text>
            </View>
            <View style={styles.bv_h4}>
              <TouchableOpacity
                style={styles.bv_touch_accept}
                onPress={() => requestHandler('1', item)}>
                <Text style={styles.bv_t4}>ACCEPT</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.bv_touch_decline}
                onPress={() => requestHandler('2', item)}>
                <Text style={styles.bv_t4}>DECLINE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ))}
    </>
  );

  const optionView = (title, source, key) => (
    <TouchableOpacity
      style={styles.opt_touch}
      onPress={() => optionHandler(key)}>
      <ImageBackground
        style={styles.opt_bg}
        source={require('../assets/background.png')}>
        <Image source={source} style={styles.opt_image} />
        <Text style={styles.opt_title}>{title}</Text>
      </ImageBackground>
    </TouchableOpacity>
  );
  const switchView = (key) => (
    <Switch
      barHeight={18}
      value={state[key]}
      onValueChange={(value) => updateStatus(key, value)}
      circleSize={18}
      circleBorderWidth={0.5}
      innerCircleStyle={styles.switch_icStyle}
      switchLeftPx={2}
      switchRightPx={2}
      switchWidthMultiplier={2.5}
      switchBorderRadius={20}
      activeTextStyle={styles.switch_text}
      inactiveTextStyle={styles.switch_text}
    />
  );

  const updateStatus = async (key, value) => {
    toggleLoading(true);
    const {status = false} = await StatusApi({
      user_id: state.user_id,
      for: key,
      what: value ? '1' : '0',
    });
    if (status) {
      setState({...state, [key]: value, isLoading: false});
    } else {
      toggleLoading(false);
      alert('Something went wrong Please try again');
    }
  };
  const toggleLoading = (isLoading) => setState({...state, isLoading});
  const optionHandler = (key) => {
    console.log('key', key);
    switch (key) {
      case 'profile':
        navigation.navigate('ProfileScreen');
        break;
      case 'astrologer':
        navigation.navigate('PremiumAstrologer');
        break;
      case 'chat':
        navigation.navigate('ChatHistory');
        break;
      case 'setting':
        navigation.navigate('SettingScreen');
        break;
      case 'earning':
        navigation.navigate('TotalEarning');
        break;
      case 'horoscope':
        navigation.navigate('HoroscopeOrderedScreen');
        break;
      case 'pooja':
        navigation.navigate('OnlinePuja');
        break;
      case 'call':
        navigation.navigate('CallHistory');
        break;
      case 'video':
        navigation.navigate('VideoCallHistory');
        break;
      case 'logout':
        logoutHandler();
        break;
      default:
        console.log('somthing missing in switch');
    }
  };
  const logoutHandler = () => {
    dispatch(actions.Logout());
    navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  };

  useEffect(() => {
    // console.log(JSON.stringify(store.getState(), null, 2));
    requestCameraAndAudioPermission().then((_) => {
      console.log('requested!');
    });

    const {token, id, os} = store.getState().deviceInfo;
    const body = {
      user_id: state.user_id,
      device_token: token,
      device_id: id,
      device_type: os,
    };
    // console.log(body);
    let timer;
    const callback = async () => {
      let ms = 5000;
      if (netInfo.isInternetReachable && state.app_status && state.approved) {
        const result = await DynamicApi(body);
        // console.log('api dynamic');
        setResultState({...resultState, ...result});
      }
      timer = setTimeout(callback, ms);
    };
    callback();

    return () => {
      console.log('HomeScreen Unmount');
      clearTimeout(timer);
    };
  }, []);

  useEffect(() => {
    console.log(JSON.stringify(resultState, null, 2));
    const {
      status = false,
      request_array,
      booking_id = 0,
      chat_g_id,
      user_id,
      name,
      booking_type,
    } = resultState;

    if (status && booking_id !== 0) {
      console.log('booking');
      if (isFocused) {
        switch (booking_type) {
          case 'chat':
            Global.bookingid = chat_g_id;
            Global.another = user_id;
            Global.user_id = state.user_id;
            Global.myname = user.name;
            console.log('Global.myname', Global.myname);
            navigation.navigate('MyChat');
            break;
          case 'audio':
            navigation.navigate('AudioCall', {
              uid: Math.floor(Math.random() * 100),
              clientRole: Host,
              channelName: chat_g_id,
              // onCancel: (message) => {},
            });
            break;
          case 'video':
            navigation.navigate('VideoCall', {
              uid: Math.floor(Math.random() * 100),
              clientRole: Host,
              channelName: chat_g_id,
              onCancel: (message) => {},
            });
            break;
        }
      }
    }
    if (booking_id === 0 && !isFocused) {
      console.log('goBack');
      EventRegister.emit('pujaend', 'it works!!!');
    }
  }, [
    resultState.status,
    resultState.booking_id,
    resultState.request_array.length,
  ]);
  useEffect(() => {
    (async () => {
      const body = {user_id: state.user_id};
      console.log('body', body);
      const {status = false, user_detail = []} = await HomeApi(body);
      if (status && user_detail.length !== 0) {
        const {video_status, audio_status, chat_status} = user_detail;
        setState({
          ...state,
          video: video_status === '1',
          audio: audio_status === '1',
          chat: chat_status === '1',
        });
      }
    })();
    (async () => {
      const {status = false, list = []} = await MasterSpecializationApi();
      if (status) {
        dispatch(actions.SkillList(list));
      }
    })();
  }, []);

  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {state.isLoading && <Loader />}
      {headerView()}
      <ScrollView>
        {statusView()}
        {resultState.request_array.length !== 0 && bookingView()}
        <View style={styles.opt_container}>
          {optionView(
            'My Profile',
            require('../assets/profileimg.png'),
            'profile',
          )}
          {optionView(
            'Astrologer',
            require('../assets/calen.png'),
            'astrologer',
          )}
          {optionView('Chat', require('../assets/chat.png'), 'chat')}
          {optionView('Call', require('../assets/call.png'), 'call')}
          {optionView('Video Call', require('../assets/video.png'), 'video')}
          {optionView(
            'Online Puja',
            require('../assets/onlinePuja.png'),
            'pooja',
          )}
          {optionView(
            'Horoscope',
            require('../assets/horoscope.png'),
            'horoscope',
          )}
          {optionView(
            'Total Earning',
            require('../assets/total.png'),
            'earning',
          )}
          {optionView('Setting', require('../assets/settings.png'), 'setting')}
          {optionView('Logout', require('../assets/logout.png'), 'logout')}
        </View>
      </ScrollView>
    </SafeAreaProvider>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  header_container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    paddingTop: 50,
  },
  header_icon: {
    height: 40,
    width: 40,
  },
  header_icon_view: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 5,
  },
  header_titleView: {
    paddingHorizontal: 10,
  },
  header_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#FFFFFF',
  },
  header_text_2: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#FFFFFF',
    textTransform: 'uppercase',
  },
  header_notifyTouch: {
    marginLeft: 'auto',
  },
  header_notifyIcon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  sv_container: {
    margin: 20,
    padding: 15,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  sv_h1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  sv_t1: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 15,
    color: '#000000',
  },
  sv_t2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#687080',
  },
  bv_title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#1D1E2C',
    marginHorizontal: 20,
  },
  bv_container: {
    margin: 20,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  bv_h1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: '#9797971a',
  },
  bv_h2: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  bv_h3: {
    paddingHorizontal: 15,
  },
  bv_h4: {
    marginLeft: 'auto',
  },
  bv_image: {
    width: 60,
    height: 60,
    borderRadius: 30,
    resizeMode: 'contain',
  },
  bv_touch_accept: {
    backgroundColor: '#00B05F',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginVertical: 4,
  },
  bv_touch_decline: {
    backgroundColor: '#ff3a31',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginVertical: 4,
  },
  bv_t1: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#F97012',
  },
  bv_t2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#69707F',
  },
  bv_t3: {
    fontFamily: 'Avenir-Medium',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#212121',
  },
  bv_t4: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#FFFFFF',
  },
  opt_container: {
    margin: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  opt_touch: {
    borderRadius: 15,
    marginHorizontal: 10,
    marginBottom: 5,
    width: (width - 100) / 3,
    height: 90,
  },
  opt_bg: {
    flex: 1,
    padding: 10,
    paddingTop: 20,
    justifyContent: 'space-between',
  },
  opt_image: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  opt_title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#000000',
  },
  switch_icStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  switch_text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 9,
    fontWeight: '900',
    color: '#fff',
  },
});

import React, {useState} from 'react';
import {TextInput, View, Text, TouchableOpacity, Image} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {globStyle} from '../styles/style';
import {SubmitButton} from '../utils/Button';
import {useDispatch, useStore} from 'react-redux';
import {
  AsyncStorageSetUserId,
  CheckStatusApi,
  GetProfileApi,
  SignInApi,
} from '../service/Api';
import * as actions from '../redux/actions';
import Loader from '../utils/Loader';

const Login = ({navigation}) => {
  const [state, setState] = useState({
    // mobile: '6393953549',
    mobile: '8010478716',
    password: '123456',
    isLoading: false,
  });
  const dispatch = useDispatch();
  const store = useStore();
  const setLoading = (isLoading) => setState({...state, isLoading});
  const forgotHandler = () => navigation.navigate('ForgotPassword');

  const loginHandler = async () => {
    const alertMessage = (msg) =>
      alert(msg || 'Something went wrong Please try again');
    console.log('loginhandler');
    const {mobile, password} = state;
    if (mobile === '') {
      alert('Please enter mobile number or registered email address');
      return;
    }
    if (password === '') {
      alert('Please enter your password');
      return;
    }
    setLoading(true);
    const {deviceInfo} = store.getState();
    const body = {
      phone: mobile,
      password: password,
      device_id: deviceInfo.id,
      device_type: deviceInfo.os,
      device_token: deviceInfo.token,
    };

    // console.log(JSON.stringify(body, null, 2));
    const {status = false, user_detail = {}, msg} = await SignInApi(body);
    // setLoading(false);
    if (status && Object.keys(user_detail).length !== 0) {
      const user_id = +user_detail.user_id;
      if (user_id !== 0) {
        AsyncStorageSetUserId(user_id.toString());
        const {status = false, user_details = {}} = await GetProfileApi({
          user_id: user_id,
        });
        if (status && Object.keys(user_details).length !== 0) {
          const {app_status = '0', approved = '0'} = await CheckStatusApi({
            user_id,
          });
          dispatch(
            actions.Login({
              ...user_details,
              app_status: app_status === '1',
              approved: approved === '1',
            }),
          );
          setLoading(false);
          navigation.replace('HomeScreen');
        } else {
          setLoading(false);
        }
      } else {
        setLoading(false);
        alertMessage('Invalid Account');
      }
    } else {
      setLoading(false);
      alertMessage(msg);
    }
  };

  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {state.isLoading && <Loader />}
      <Image
        style={{
          height: 110,
          width: 145,
          marginTop: 35,
          justifyContent: 'center',
          alignSelf: 'center',
        }}
        source={require('../assets/splogo.png')}
      />
      <Text
        style={{
          marginLeft: 10,
          fontSize: 72,
          marginTop: 37,
          color: '#F7F7FB',
        }}>
        LOGIN
      </Text>

      <View style={{width: '60%', marginLeft: 32}}>
        <Text
          style={{
            fontSize: 24,
            marginTop: -52,
            color: '#1D1E2C',
            fontWeight: 'bold',
          }}>
          Welcome to Shaktipeeth Digital!
        </Text>
      </View>

      <KeyboardAwareScrollView>
        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              placeholder="Mobile Number/Email"
              keyboardType="default"
              value={state.mobile}
              onChangeText={(mobile) => setState({...state, mobile})}
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              placeholder="Password"
              secureTextEntry={true}
              value={state.password}
              onChangeText={(password) => setState({...state, password})}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={forgotHandler}
          style={{
            marginVertical: 10,
            marginRight: 20,
            padding: 5,
            alignSelf: 'flex-end',
          }}>
          <Text
            style={{
              fontFamily: 'Avenir-Medium',
              fontSize: 14,
              color: '#69707F',
            }}>
            Forgot Password?
          </Text>
        </TouchableOpacity>
        {SubmitButton('Login', loginHandler)}
      </KeyboardAwareScrollView>
    </SafeAreaProvider>
  );
};

export default Login;

import React, {useEffect} from 'react';
import {ImageBackground, Image, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {
  AsyncStorageGetUserId,
  GetProfileApi,
  CheckStatusApi,
} from '../service/Api';
import * as actions from '../redux/actions';
import {StatusBarLight} from '../utils/CustomStatusBar';

const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  const screenHandler = async () => {
    const user_id = await AsyncStorageGetUserId();
    console.log(user_id);
    if (user_id && user_id !== '') {
      console.log('pass');
      console.log(
        JSON.stringify(
          await GetProfileApi({
            user_id: user_id,
          }),
        ),
      );
      const {status = false, user_details = {}} = await GetProfileApi({
        user_id: user_id,
      });
      if (status && Object.keys(user_details).length !== 0) {
        const {app_status = '0', approved = '0'} = await CheckStatusApi({
          user_id,
        });
        dispatch(
          actions.Login({
            ...user_details,
            app_status: app_status === '1',
            approved: approved === '1',
          }),
        );
        navigation.replace('HomeScreen', {
          app_status: app_status === '1',
          approved: approved === '1',
        });
        return;
      }
    }
    navigation.replace('Login');
  };

  useEffect(() => {
    setTimeout(() => {
      screenHandler();
    }, 2000);
  }, []);
  return (
    <View style={{flex: 1}}>
      <StatusBarLight />
      <ImageBackground
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        source={require('../assets/astroback.png')}>
        <Image
          style={{
            height: 180,
            width: 235,
            resizeMode: 'contain',
          }}
          source={require('../assets/shaktilogo.png')}
        />
      </ImageBackground>
    </View>
  );
};

export default Splash;

import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  FlatList,
  StyleSheet,
} from 'react-native';

import React, {useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {SimpleHeader} from '../utils/Header';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {globStyle} from '../styles/style';
import {useStore} from 'react-redux';
import {ChatHistoryApi} from '../service/Api';
import Global from './Global';
import Loader from '../utils/Loader';

const ChatHistory = ({navigation}) => {
  const store = useStore();
  const [state, setState] = useState({
    user_id: store.getState().user.user_id,
    history: [],
    isLoading: true,
  });

  const renderItem1 = ({item, index}) => {
    // alert(JSON.stringify(item))
    return (
      <View style={{flexDirection: 'column', width: '100%', marginTop: 20}}>
        <TouchableOpacity
          onPress={() => {
            Global.bookingid = item.chat_group_id;
            Global.another = item.reciever_id;
            Global.user_id = state.user_id;
            navigation.navigate('MyChat');
          }}>
          <View
            style={{
              width: '100%',
              justifyContent: 'space-between',
              alignSelf: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                paddingHorizontal: 20,
              }}>
              <Image
                style={{height: 60, width: 60, resizeMode: 'contain'}}
                source={{uri: item.receiver_image}}
              />

              <View style={{flexDirection: 'column', marginLeft: 15}}>
                <Text
                  style={{
                    fontFamily: 'Avenir',
                    fontSize: 18,
                    fontFamily: 'Avenir',
                    color: '#1E1F20',
                  }}>
                  {item.sender_name}
                </Text>

                <Text
                  style={{
                    fontFamily: 'Avenir',
                    fontSize: 15,
                    fontFamily: 'Avenir',
                    color: '#979797',
                  }}>
                  {item.message}
                </Text>
              </View>
            </View>

            <Text
              style={{
                marginTop: -15,
                fontFamily: 'Avenir',
                fontSize: 15,
                fontFamily: 'Avenir',
                color: 'white',
                marginLeft: 20,
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: 'red',
                padding: 5,
                textAlign: 'center',
                marginRight: 20,
              }}>
              {item.total_unread_messages}
            </Text>
          </View>
        </TouchableOpacity>

        <View
          style={{
            width: '100%',
            borderWidth: 0.3,
            borderColor: '#D1D1D1',
            marginTop: 10,
          }}></View>
      </View>
    );
  };

  const _keyExtractor = (item, index) => item.key;
  useEffect(() => {
    fetchList();
  }, []);
  const fetchList = async () => {
    const {user_id} = state;
    const body = {
      user_id,
      // schedule_date: '',
      // schedule_date_end: '',
    };
    console.log(body);
    // const {status = false, lists = []} = await ChatHistoryApi(body);
    const {status = false, history = []} = await ChatHistoryApi(body);
    if (status) {
      console.log(JSON.stringify(history, null, 2));
      setState({...state, history, isLoading: false});
    } else {
      setState({...state, isLoading: false});
    }
  };

  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {state.isLoading && <Loader />}
      {SimpleHeader('Chat History', () => navigation.goBack())}
      {state.history.length === 0 && !state.isLoading && (
        <Text style={styles.noResult}>No Chat History Found</Text>
      )}
      <FlatList
        data={state.history}
        horizontal={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={_keyExtractor}
        renderItem={renderItem1}
      />
    </SafeAreaProvider>
  );
};

export default ChatHistory;

const styles = StyleSheet.create({
  noResult: {
    marginTop: '50%',
    alignSelf: 'center',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
  },
});

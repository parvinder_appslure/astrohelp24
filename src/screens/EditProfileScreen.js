import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  TextInput,
  StyleSheet,
  FlatList,
} from 'react-native';

import React, {useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckBox from '@react-native-community/checkbox';
import {SimpleHeader} from '../utils/Header';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SubmitButton} from '../utils/Button';
import {useDispatch, useStore} from 'react-redux';
import {GetProfileApi, UpdateProfileApi} from '../service/Api';
import * as actions from '../redux/actions';
const EditProfileScreen = ({navigation}) => {
  const store = useStore();
  const {user, skillList} = store.getState();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    mobile: user.mobile,
    dob: user.dob,
    consultType: '',
    experience: '',
    name: user.name,
    email: user.email,
    gender: user.gender,
    consultation_type: user.consultation_type,
    skills: user.skills.map((item) => item.skill_name).join('|'),
    language: user.language,
    experience: user.experience,
    isLoading: false,
    skillSelect: user.skills,
    skillList,
  });

  const [modalState, setModalState] = useState(false);
  const inputView = (
    label,
    key,
    placeholder,
    keyboardType = 'default',
    editiable = true,
  ) => (
    <>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.outerView}>
        <TextInput
          style={styles.textInput}
          editable={editiable}
          keyboardType="default"
          placeholder={placeholder}
          keyboardType={keyboardType}
          onChangeText={(value) => setState({...state, [key]: value})}
          value={state[key]}
        />
        <Image
          source={require('../assets/edit1.png')}
          style={styles.editImage2}
        />
      </View>
    </>
  );
  const saveHandler = async () => {
    const {user_id} = user;
    const {
      consultation_type,
      gender,
      name,
      dob,
      skills,
      experience,
      language,
      skillSelect,
    } = state;
    const body = {
      user_id,
      name,
      dob,
      gender,
      skills: skillSelect.map((item) => item.skill_id).join('|'),
      language,
      experience,
      service_offered: 'Test',
      specialization: 'Test',
    };
    if (body.name === '') {
      alert('Please Enter Your Name');
      return;
    }
    if (body.dob === '') {
      alert('Please Enter Your Dob (YYYY-MM-DD)');
      return;
    }
    if (body.gender === '') {
      alert('Please Enter Your Gender (Male or Female)');
      return;
    }
    if (body.skills === '') {
      alert('Please Enter Your Skills');
      return;
    }
    if (body.experience === '') {
      alert('Please Enter Your Experience in Years');
      return;
    }
    setState({...state, isLoading: true});
    const {status = false} = await UpdateProfileApi(body);
    setState({...state, isLoading: false});
    if (status) {
      updateProfile(body.user_id);
      alert('Your Profile has been Updated Successfull');
    } else {
      alert('Something went wrong');
    }
  };
  const updateProfile = async (user_id) => {
    const {status = false, user_details = {}} = await GetProfileApi({
      user_id: user_id,
    });
    if (status) {
      dispatch(actions.Login(user_details));
    }
  };
  const modalSubmitHandler = () => {
    setModalState(false);
    const {skillList} = state;
    const skillSelect = [];
    skillList.map(({flag, id, name}) => {
      if (flag)
        skillSelect.push({
          skill_id: id,
          skill_name: name,
        });
    });
    setState({
      ...state,
      skillSelect,
      skills: skillSelect.map((item) => item.skill_name).join('|'),
    });
  };
  const modalView = () => (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalState}
      onRequestClose={() => {
        Alert.alert('Modal has been closed.');
      }}>
      <View style={modalStyle.container}>
        <View style={modalStyle.modalView}>
          <Text style={styles.chk_title}>Select Skills</Text>
          <FlatList
            data={state.skillList}
            renderItem={({item}) => (
              <View style={styles.chk_view}>
                <CheckBox
                  tintColors={{true: '#FA9219', false: '#242E42'}}
                  value={item.flag}
                  onValueChange={(flag) => {
                    const {skillList} = state;
                    skillList.map((obj) => {
                      if (obj.id === item.id) obj.flag = flag;
                    });
                    setState({...state, skillList});
                  }}
                />
                <Text style={styles.chk_subTitle}>{item.name}</Text>
              </View>
            )}
          />
          <View style={styles.ct_conatiner}>
            <TouchableOpacity
              style={styles.ct_touchCancel}
              onPress={() => {
                setModalState(false);
                listUpdate();
              }}>
              <Text style={styles.ct_text_1}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.ct_touchDone}
              onPress={modalSubmitHandler}>
              <Text style={styles.ct_text_2}>Done</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
  const listUpdate = () => {
    const list = [];
    const {skillList, skillSelect} = state;
    skillSelect.map(({skill_id}) => list.push(skill_id));
    skillList.map((item) => (item.flag = list.includes(item.id)));
    setState({...state, skillList});
  };
  useEffect(() => {
    listUpdate();
  }, []);
  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {modalView()}
      {SimpleHeader('Edit Profile', () => navigation.goBack())}

      <View style={styles.container}>
        <KeyboardAwareScrollView>
          {inputView('Name', 'name')}
          {inputView('Email', 'email', '', '', false)}
          {inputView('Mobile', 'mobile', '', 'number-pad', false)}
          {inputView('Gender', 'gender', 'Male or Female', '')}
          {inputView('D.O.B', 'dob', 'DD-MM-YYYY', 'number-pad')}
          {inputView('Consultand Type', 'consultation_type', '', '', false)}
          <TouchableOpacity onPress={() => setModalState(true)}>
            {inputView('Skill', 'skills', '', '', false)}
          </TouchableOpacity>
          {inputView('Language (Hindi|English)', 'language', 'Hindi|English')}
          {inputView('Experience (in years)', 'experience')}
          {SubmitButton('SAVE', saveHandler)}
        </KeyboardAwareScrollView>
      </View>
    </SafeAreaProvider>
  );
};

export default EditProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 20,
  },
  label: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 20,
    color: '#69707F',
    marginHorizontal: 30,
  },
  textInput: {
    flex: 1,
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'center',
  },
  outerView: {
    flexDirection: 'row',
    borderBottomColor: '#EAEAEA',
    borderBottomWidth: 1,
    marginBottom: 20,
    width: '82%',
    alignSelf: 'center',
  },
  editImage2: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginHorizontal: 5,
  },
  chk_view: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  chk_title: {
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'center',
    marginTop: 5,
    marginBottom: 15,
    textDecorationLine: 'underline',
  },
  chk_subTitle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#000000',
  },
  ct_conatiner: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingBottom: 10,
  },
  ct_text_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#FA9219',
  },
  ct_text_2: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#fff',
  },
  ct_touchCancel: {
    borderColor: '#FA9219',
    borderWidth: 1,
    borderRadius: 25,
    width: '30%',
    paddingVertical: 5,
    alignItems: 'center',
  },
  ct_touchDone: {
    backgroundColor: '#FA9219',
    borderRadius: 25,
    width: '30%',
    paddingVertical: 5,
    alignItems: 'center',
  },
});
const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
  },
  modalView: {
    borderRadius: 8,
    backgroundColor: 'white',
    marginHorizontal: 30,
    padding: 10,
    height: '40%',
  },
});

import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  FlatList,
  StyleSheet,
} from 'react-native';

import React, {useState, useEffect} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import {useStore} from 'react-redux';
import {HoroscopeHistoryApi} from '../service/Api';
import Loader from '../utils/Loader';

const HoroscopeOrderedScreen = ({navigation}) => {
  const store = useStore();
  const [state, setState] = useState({
    lists: [],
    isLoading: true,
  });
  const toggleLoading = (isLoading) => setState({...state, isLoading});
  const listView = (key, value) => (
    <View style={styles.ft_view}>
      <Text style={styles.ft_text_1}>{key}</Text>
      <Text style={styles.ft_text_2}>{value}</Text>
    </View>
  );
  const renderItem = ({item}) => (
    <TouchableOpacity
      key={`key_${item.id}`}
      style={styles.ft_touch}
      onPress={() =>
        item.status === '0'
          ? navigation.navigate('HoroscopeDetail', item)
          : null
      }>
      {listView('Name', item.name)}
      {listView('Date of Birth', item.user_dob)}
      {listView('Place of Birth', item.user_pob)}
      {listView('Problem Area', item.problem_area)}
      {listView('Post Date', item.added_on)}
      {listView('Status', item.stat_us)}
      {/* {listView('Status', item.status)} */}
    </TouchableOpacity>
  );

  useEffect(() => {
    fetchListData();
  }, []);

  const fetchListData = async () => {
    // console.log(store.getState().user.user_id);
    const {status = false, lists = []} = await HoroscopeHistoryApi({
      user_id: store.getState().user.user_id,
    });
    if (status) {
      setState({...state, lists, isLoading: false});
      // console.log(JSON.stringify(lists, null, 2));
    } else {
      toggleLoading(false);
    }
  };

  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {state.isLoading && <Loader />}
      {SimpleHeader('Horoscope Ordered', () => navigation.goBack())}
      <View style={styles.mainView}>
        {state.lists.length === 0 && !state.isLoading && (
          <Text style={styles.noResult}>No order Found</Text>
        )}
        {state.lists.length !== 0 && (
          <FlatList
            data={state.lists}
            horizontal={false}
            contentContainerStyle={{
              paddingVertical: 20,
            }}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem}
          />
        )}
      </View>
    </SafeAreaProvider>
  );
};

export default HoroscopeOrderedScreen;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
  },
  ft_touch: {
    margin: 20,
    marginBottom: 0,
    paddingVertical: 10,
    borderRadius: 5,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  ft_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    paddingHorizontal: 20,
  },
  ft_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#83878E',
  },
  ft_text_2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#000000',
  },
  noResult: {
    marginTop: '50%',
    alignSelf: 'center',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
  },
});

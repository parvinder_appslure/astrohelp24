import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  StyleSheet,
  Dimensions,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import React, {useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import {Appbar, Menu, Provider} from 'react-native-paper';
import {useStore} from 'react-redux';
import Loader from '../utils/Loader';
import {EarningApi, textInPrice} from '../service/Api';

import {Calendar} from 'react-native-calendars';
import moment from 'moment';
const TotalEarning = ({navigation}) => {
  const store = useStore();
  const [state, setstate] = useState({
    total_income: '0',
    list: [],
    condition: '10 data',
    start_date: '',
    end_date: '',
    user_id: store.getState().user.user_id,
    isLoading: true,
  });
  const [menuVisible, setMenuVisible] = useState(false);
  const [calState, setCalState] = useState({
    visible: false,
    markedDates: {},
    selectType: 1,
  });
  const renderItem1 = ({item}) => (
    <View
      key={`ft_${item.id}`}
      style={{
        flexDirection: 'column',
        width: '90%',
        marginTop: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
      }}>
      <TouchableOpacity>
        <View
          style={{
            width: '90%',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
              width: '83%',
            }}>
            <Image
              style={{
                height: 50,
                width: 50,
                resizeMode: 'contain',
                marginTop: -5,
              }}
              source={require('../assets/total.png')}
            />

            <View
              style={{flexDirection: 'column', marginLeft: 15, width: '70%'}}>
              <Text
                style={{
                  fontFamily: 'Avenir',
                  fontSize: 16,
                  fontFamily: 'Avenir',
                  color: '#1E1F20',
                }}>
                Payment Recieved from:
              </Text>
              <Text
                style={{
                  fontFamily: 'Avenir',
                  fontSize: 16,
                  fontFamily: 'Avenir',
                  color: '#1E1F20',
                }}>
                {item.user_name}
              </Text>

              <Text
                style={{
                  fontFamily: 'Avenir',
                  fontSize: 15,
                  fontFamily: 'Avenir',
                  color: '#979797',
                }}>
                {item.schedule_date}
              </Text>
            </View>
          </View>

          <Text
            style={{
              marginTop: -15,
              fontFamily: 'Avenir',
              fontSize: 15,
              fontFamily: 'Avenir',
              color: '#F97012',
              marginLeft: 20,
            }}>
            {`+ ${textInPrice(item.payable_amount)}`}
          </Text>
        </View>
      </TouchableOpacity>

      <View
        style={{
          width: '100%',
          borderWidth: 0.3,
          borderColor: '#D1D1D1',
          marginTop: 10,
        }}></View>
    </View>
  );

  useEffect(() => {
    const {condition} = state;
    if (condition) {
      fetchListData();
    }
  }, [state.condition]);

  useEffect(() => {
    const {start_date, end_date} = state;
    const marks = {
      selected: true,
      selectedColor: '#09304B',
    };
    let markedDates = {};
    if (start_date !== '') {
      markedDates[start_date] = marks;
    }
    if (end_date !== '') {
      markedDates[end_date] = marks;
    }
    setCalState({...calState, markedDates});
  }, [state.start_date, state.end_date]);

  const setCondition = (condition) => {
    setMenuVisible(false);
    if (condition) {
      setstate({...state, condition});
    } else {
      setstate({...state, condition, start_date: '', end_date: ''});
      setCalState({...calState, visible: true});
    }
  };

  const fetchListData = async () => {
    const {user_id, condition, start_date, end_date} = state;
    const body = {
      user_id,
      condition,
      start_date,
      end_date,
    };
    console.log(JSON.stringify(body.condition, null, 2));
    setstate({...state, isLoading: true});
    const {status = false, list = [], total_income = '0'} = await EarningApi(
      body,
    );
    console.log('res status', status);
    if (status) {
      setstate({
        ...state,
        list,
        total_income,
        isLoading: false,
      });
    } else {
      setstate({...state, isLoading: false});
    }
  };

  const onDayPressHandler = ({dateString}) => {
    let {start_date, end_date} = state;
    const {selectType} = calState;
    if (selectType === 1) {
      start_date = start_date === dateString ? '' : dateString;
    } else {
      end_date = end_date === dateString ? '' : dateString;
    }
    setstate({...state, start_date, end_date});
  };

  const cancelHandler = () => {
    setCalState({...calState, visible: false});
    setCondition('10 data');
  };
  const searchHandler = () => {
    setCalState({...calState, visible: false});
    const {start_date, end_date} = state;
    if (start_date !== '' && end_date !== '') {
      fetchListData();
    }
  };
  const calendarView = () => (
    <View style={calStyle.container}>
      <View style={calStyle.container_2}>
        <View style={calStyle.view_1}>
          <TouchableOpacity
            style={
              calStyle[calState.selectType === 1 ? 'touch_active' : 'touch']
            }
            onPress={() => setCalState({...calState, selectType: 1})}>
            <Text
              style={
                calStyle[calState.selectType === 1 ? 'text_1' : 'text_2']
              }>{`Start From : ${state.start_date || '- - -'}`}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              calStyle[calState.selectType === 2 ? 'touch_active' : 'touch']
            }
            onPress={() => setCalState({...calState, selectType: 2})}>
            <Text
              style={
                calStyle[calState.selectType === 2 ? 'text_1' : 'text_2']
              }>{`End From : ${state.end_date || '- - -'}`}</Text>
          </TouchableOpacity>
        </View>
        <Calendar
          onDayPress={onDayPressHandler}
          markedDates={calState.markedDates}
          markingType={'interactive'}
          maxDate={moment().format('YYYY-MM-DD')}
        />
        <View style={calStyle.view_2}>
          <TouchableOpacity
            style={calStyle.touch_cancel}
            onPress={cancelHandler}>
            <Text style={calStyle.text_cancel}>Cancel</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={calStyle.touch_search}
            onPress={searchHandler}>
            <Text style={calStyle.text_search}>Search</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
  return (
    <Provider>
      <SafeAreaProvider style={globStyle.safeAreaView}>
        <StatusBarLight />
        {SimpleHeader('Total Earning', () => navigation.goBack())}
        {state.isLoading && <Loader />}
        <View
          style={{
            flexDirection: 'column',
            width: '90%',
            backgroundColor: '#F97012',
            height: 152,
            justifyContent: 'center',
            marginTop: 20,
            alignSelf: 'center',
            borderRadius: 10,
          }}>
          <Text
            style={{
              fontSize: 45,
              fontFamily: 'Avenir',
              color: '#FFF',
              fontWeight: 'bold',
              alignSelf: 'center',
            }}>
            {textInPrice(state.total_income)}
          </Text>

          <Text
            style={{
              fontSize: 18,
              fontFamily: 'Avenir',
              color: '#FFF',
              fontWeight: 'bold',
              alignSelf: 'center',
            }}>
            Total Earnings
          </Text>
        </View>

        <View
          style={{
            width: '90%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignSelf: 'center',
            marginTop: 25,
          }}>
          <Text
            style={{
              lineHeight: 34,
              fontSize: 30,
              fontWeight: 'normal',
              color: '#000',
              fontFamily: 'Avenir',
            }}>
            Earning History
          </Text>

          <TouchableOpacity onPress={() => setMenuVisible(true)}>
            <Image
              style={{height: 25, width: 25, resizeMode: 'contain'}}
              source={require('../assets/whiteCalendar.png')}
            />
          </TouchableOpacity>
          <View style={styles.menuView}>
            <Menu
              contentStyle={styles.menu}
              visible={menuVisible}
              onDismiss={() => setMenuVisible(false)}
              anchor={<Appbar.Action onPress={() => setMenuVisible(true)} />}>
              <Menu.Item
                titleStyle={styles.menuTitleStyle}
                onPress={() => setCondition('10 data')}
                title="Last 10 Consultations"
              />
              <Menu.Item
                titleStyle={styles.menuTitleStyle}
                onPress={() => setCondition('7 data')}
                title="View Last week"
              />
              <Menu.Item
                titleStyle={styles.menuTitleStyle}
                onPress={() => setCondition('30 data')}
                title="View Last month"
              />
              <Menu.Item
                titleStyle={styles.menuTitleStyle}
                onPress={() => setCondition('')}
                title="Custom Dates"
              />
            </Menu>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignSelf: 'center',
            marginBottom: 120,
          }}>
          <FlatList
            style={{width: '100%'}}
            data={state.list}
            horizontal={false}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.id.toString()}
            renderItem={renderItem1}
          />
        </View>
        {!state.isLoading && state.list.length === 0 && (
          <Text style={styles.nobooking}>No booking found</Text>
        )}
        {calState.visible && calendarView()}
      </SafeAreaProvider>
    </Provider>
  );
};

export default TotalEarning;
const styles = StyleSheet.create({
  menu: {
    marginTop: 60,
    marginLeft: -10,
  },
  menuView: {
    position: 'absolute',
    right: 5,
  },
  menuTitleStyle: {
    fontFamily: 'Muli',
    fontWeight: '600',
    fontSize: 12,
    color: '#344356',
  },
  nobooking: {
    alignSelf: 'center',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: 'black',
  },
});
const calStyle = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.7)',
    width: width,
    height: height,
    flexDirection: 'column-reverse',
  },
  container_2: {
    width: window.width,
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    overflow: 'hidden',
    paddingVertical: 20,
  },
  view_1: {
    flexDirection: 'row',
    marginVertical: 5,
    marginHorizontal: 20,
    justifyContent: 'space-evenly',
  },
  touch_active: {
    flex: 0.47,
    padding: 5,
    backgroundColor: '#F97012',
    justifyContent: 'flex-start',
  },
  touch: {
    flex: 0.47,
    padding: 5,
    justifyContent: 'flex-start',
  },
  text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: 'white',
  },
  text_2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: 'black',
  },
  view_2: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 10,
  },
  touch_cancel: {
    backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 25,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'gray',
  },
  touch_search: {
    backgroundColor: '#F97012',
    paddingVertical: 5,
    paddingHorizontal: 25,
    borderRadius: 15,
  },
  text_cancel: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: 'black',
  },
  text_search: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: 'white',
  },
});
